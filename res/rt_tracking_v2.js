var _ra_dyn_sub = {
    status: 0
};
var _ra_prod_recom = {
    status: 0
};
_ra_prod_recom.status = 1;
_ra_prod_recom.settings = [];
_ra_prod_recom.settings[0] = {};
_ra_prod_recom.settings[0].type = 1;
_ra_prod_recom.settings[0].next_pages = 5;
_ra_prod_recom.settings[0].display_seconds = 2;
_ra_prod_recom.settings[0].stop_on_close = 7;
_ra_req_folder = "v2vsv3-rtgt.rhcloud.com";
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '')
    }
}
navigator.sayswho = (function() {
    var ua = navigator.userAgent,
        tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '')
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
        if (tem != null) return 'Opera ' + tem[1]
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1])
    }
    return M.join(' ')
})();
var _ra_browser = navigator.sayswho;
if (typeof _ra_browser != "string") {
    _ra_browser = ""
}
var _ra_dyn_sub = _ra_dyn_sub || {};
var _ra_prod_recom = _ra_prod_recom || {};
var _ra_temp = _ra || {};
var _ra = {
    ready: true,
    session_id: undefined,
    currentUrl: location.href.replace(location.hash, "").replace("#", ""),
    lastUrl: undefined,
    browser: _ra_browser,
    request: undefined,
    requestLiveTr: undefined,
    send: [],
    callbacks: ["no_callback"],
    sendLiveTr: [],
    arr_buffer: [],
    buffer_semaphore: 'go',
    prodId: undefined,
    scrollSended: false,
    secSpent: 0,
    secSpentInt: undefined,
    dblClickPriceSent: false,
    emailFromCookie: false,
    liveTr: {
        progress: false,
        count: 0,
        counted: false,
        loaded: false,
        stopped: false,
        dynSub: {
            stopped: true,
            settings: undefined
        },
        prodRecom: {
            stopped: true,
            settings: undefined,
            type: 0,
            message: 1,
            count: 0,
            prod_id: 0
        }
    },
    init: function(temp_info, dyn_sub, prod_recom) {
        //DEBUG
        this.attachSelfToDebugger();
        //DEBUG
        this.initRequestUrls(_ra_req_folder);
        this.initSessionId();
        this.setLastUrl();
        this.setEmailOrRefererFromUrl();
        if (dyn_sub !== undefined && dyn_sub.status !== undefined && dyn_sub.settings !== undefined) {
            this.liveTr.dynSub.stopped = parseInt(dyn_sub.status) == 0 ? true : false;
            var rand = this.randomInRange(0, dyn_sub.settings.length - 1);
            while (dyn_sub.settings[rand] === undefined) {
                rand = this.randomInRange(0, dyn_sub.settings.length - 1)
            }
            this.liveTr.dynSub.settings = dyn_sub.settings[rand]
        }
        if (prod_recom !== undefined && prod_recom.status !== undefined && prod_recom.settings !== undefined) {
            this.liveTr.prodRecom.stopped = parseInt(prod_recom.status) == 0 ? true : false;
            var rand = this.randomInRange(0, prod_recom.settings.length - 1);
            while (prod_recom.settings[rand] === undefined) {
                rand = this.randomInRange(0, prod_recom.settings.length - 1)
            }
            this.liveTr.prodRecom.settings = prod_recom.settings[rand];
            this.liveTr.prodRecom.settingsTemp = {};
            for (var prop in prod_recom.settings) {
                this.liveTr.prodRecom.settingsTemp[prod_recom.settings[prop].type] = prod_recom.settings[prop]
            }
        }
        if (this.liveTr.dynSub.stopped && this.liveTr.prodRecom.stopped) {
            this.liveTr.stopped = true
        }
        this.initLiveTrCookies();
        if (temp_info instanceof Object) {
            for (var prop in temp_info) {
                var _callback = undefined;
                if (prop == 'setEmailInfo') {
                    if (temp_info.setEmailInfo.callback_function !== undefined) {
                        _callback = temp_info.setEmailInfo.callback_function
                    }
                    this.setEmail(temp_info.setEmailInfo, _callback)
                } else if (prop == 'sendCategoryInfo') {
                    if (temp_info.sendCategoryInfo.callback_function !== undefined) {
                        _callback = temp_info.sendCategoryInfo.callback_function
                    }
                    this.sendCategory(temp_info.sendCategoryInfo, _callback)
                } else if (prop == 'sendBrandInfo') {
                    if (temp_info.sendBrandInfo.callback_function !== undefined) {
                        _callback = temp_info.sendBrandInfo.callback_function
                    }
                    this.sendBrand(temp_info.sendBrandInfo, _callback)
                } else if (prop == 'sendProductInfo') {
                    if (temp_info.sendProductInfo.callback_function !== undefined) {
                        _callback = temp_info.sendProductInfo.callback_function
                    }
                    this.sendProduct(temp_info.sendProductInfo, _callback)
                } else if (prop == 'sendAddToCartInfo') {
                    if (temp_info.sendAddToCartInfo.callback_function !== undefined) {
                        _callback = temp_info.sendAddToCartInfo.callback_function
                    }
                    this.addToCart(temp_info.sendAddToCartInfo.product_id, temp_info.sendAddToCartInfo.variation, _callback)
                } else if (prop == 'addToCartInfo') {
                    if (temp_info.addToCartInfo.callback_function !== undefined) {
                        _callback = temp_info.addToCartInfo.callback_function
                    }
                    this.addToCart(temp_info.addToCartInfo.product_id, temp_info.addToCartInfo.variation, _callback)
                } else if (prop == "setVariationInfo") {
                    if (temp_info.setVariationInfo.callback_function !== undefined) {
                        _callback = temp_info.setVariationInfo.callback_function
                    }
                    this.setVariation(temp_info.setVariationInfo.product_id, temp_info.setVariationInfo.variation, _callback)
                } else if (prop == "addToWishlistInfo") {
                    if (temp_info.addToWishlistInfo.callback_function !== undefined) {
                        _callback = temp_info.addToWishlistInfo.callback_function
                    }
                    this.addToWishlist(temp_info.addToWishlistInfo.product_id, _callback)
                } else if (prop == "clickImageInfo") {
                    if (temp_info.clickImageInfo.callback_function !== undefined) {
                        _callback = temp_info.clickImageInfo.callback_function
                    }
                    this.clickImage(temp_info.clickImageInfo.product_id, _callback)
                } else if (prop == "commentOnProductInfo") {
                    if (temp_info.commentOnProductInfo.callback_function !== undefined) {
                        _callback = temp_info.commentOnProductInfo.callback_function
                    }
                    this.commentOnProduct(temp_info.commentOnProductInfo.product_id, _callback)
                } else if (prop == 'saveOrderInfo') {
                    if (temp_info.saveOrderInfo.callback_function !== undefined) {
                        _callback = temp_info.saveOrderInfo.callback_function
                    }
                    if (temp_info.saveOrderProducts !== undefined) {
                        this.saveOrder(temp_info.saveOrderInfo, temp_info.saveOrderProducts, _callback)
                    }
                } else if (prop == "visitHelpPageInfo") {
                    if (temp_info.visitHelpPageInfo.callback_function !== undefined) {
                        _callback = temp_info.visitHelpPageInfo.callback_function
                    }
                    if (temp_info.visitHelpPageInfo.visit !== undefined) {
                        this.visitHelpPage(_callback)
                    }
                } else if (prop == 'sendIdsInfo') {
                    this.checkoutIds(temp_info.sendIdsInfo)
                } else if (prop == "checkoutIdsInfo") {
                    this.checkoutIds(temp_info.checkoutIdsInfo)
                }
            }
        }
    },
    initRequestUrls: function(folder) {
        var here = document.location.hostname.replace('www.', '');
        if (here.indexOf(folder) > -1) {
            here = folder
        }
        this.request = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'retargeting-data.eu/' + here + '/request/ra.ev';
        this.requestLiveTr = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'retargeting-data.eu/' + here + '/request/ra_live_tr.ev'
    },
    initSessionId: function() {
        var _ra_sesid = this.getCookie('_ra_sesid');
        if (_ra_sesid.length > 0) {
            this.session_id = _ra_sesid
        }
    },
    setSessionId: function(session_id) {
        if (session_id !== undefined && (this.session_id != session_id || this.getCookie('_ra_sesid') != session_id)) {
            this.setCookie('_ra_sesid', session_id, 1);
            this.session_id = session_id
        }
    },
    resetLiveTrCookie: function() {
        var v = 'c:' + this.liveTr.count;
        v += '#p:' + (this.liveTr.progress ? this.liveTr.progress : '');
        v += '#pr_t:' + this.liveTr.prodRecom.type;
        v += '#pr_c:' + this.liveTr.prodRecom.count;
        v += '#pr_m:' + this.liveTr.prodRecom.message;
        v += '#pr_i:' + this.liveTr.prodRecom.prod_id;
        this.setCookie('_ra_lvtr', v, 1)
    },
    initLiveTrCookies: function() {
        var _ra_lvtr = this.getCookie('_ra_lvtr');
        if (_ra_lvtr.length == 0) {
            this.resetLiveTrCookie()
        }
        _ra_lvtr = this.getCookie('_ra_lvtr').split('#');
        for (var prop in _ra_lvtr) {
            var op = String(_ra_lvtr[prop]).split(':');
            if (op.length == 2) {
                if (op[0] == 'c') {
                    op[1] = parseInt(op[1]);
                    if (!isNaN(op[1]) && op[1] >= 0) {
                        this.liveTr.count = op[1]
                    }
                } else if (op[0] == 'p') {
                    if (op[1] == 'pr') {
                        this.liveTr.progress = op[1]
                    }
                } else if (op[0] == 'pr_t') {
                    op[1] = parseInt(op[1]);
                    if (!isNaN(op[1]) && op[1] > 0) {
                        this.liveTr.prodRecom.type = op[1]
                    }
                } else if (op[0] == 'pr_c') {
                    op[1] = parseInt(op[1]);
                    if (!isNaN(op[1]) && op[1] >= 0) {
                        this.liveTr.prodRecom.count = op[1]
                    }
                } else if (op[0] == 'pr_m') {
                    op[1] = parseInt(op[1]);
                    if (!isNaN(op[1]) && op[1] >= 1 && op[1] <= 3) {
                        this.liveTr.prodRecom.message = op[1]
                    }
                } else if (op[0] == 'pr_i') {
                    if (op[1] !== undefined) {
                        this.liveTr.prodRecom.prod_id = op[1]
                    }
                }
            }
        }
        this.resetLiveTrCookie();
        if (this.liveTr.progress == 'pr') {
            if (this.liveTr.prodRecom.message == 1 || (this.liveTr.prodRecom.message == 2 && this.liveTr.count > this.liveTr.prodRecom.count) || (this.liveTr.prodRecom.message == 3 && this.liveTr.count - this.liveTr.prodRecom.count > 10)) {
                this.setLiveTrProgress(false)
            } else {
                if (this.liveTr.prodRecom.type != this.liveTr.prodRecom.settings.type && this.liveTr.prodRecom.settingsTemp[this.liveTr.prodRecom.type] !== undefined) {
                    this.liveTr.prodRecom.settings = this.liveTr.prodRecom.settingsTemp[this.liveTr.prodRecom.type]
                }
            }
        } else {
            if (this.liveTr.prodRecom.count > 0) {
                this.setLiveTrProgress(false)
            }
        }
        if (this.liveTr.prodRecom.settingsTemp !== undefined) {
            delete this.liveTr.prodRecom.settingsTemp
        }
        if (!this.liveTr.dynSub.stopped && this.getCookie('_ra_ds_stopped') == "1") {
            this.liveTr.dynSub.stopped = true
        }
        if (!this.liveTr.prodRecom.stopped && this.getCookie('_ra_pr_stopped') == "1") {
            this.liveTr.prodRecom.stopped = true
        }
        if (this.liveTr.dynSub.stopped && this.liveTr.prodRecom.stopped) {
            this.liveTr.stopped = true
        }
    },
    countLiveTrPage: function() {
        if (!this.liveTr.counted) {
            this.liveTr.counted = true;
            this.liveTr.count++;
            this.resetLiveTrCookie()
        }
    },
    setLiveTrProgress: function(value) {
        value = value === undefined ? false : value;
        this.liveTr.progress = value;
        if (value === false) {
            this.liveTr.prodRecom.type = 0;
            this.liveTr.prodRecom.count = 0;
            this.liveTr.prodRecom.prod_id = 0;
            this.liveTr.prodRecom.message = 1
        }
        this.resetLiveTrCookie()
    },
    stopLiveTr: function(type) {
        if (type == 'ds') {
            this.liveTr.dynSub.stopped = true;
            this.setCookie('_ra_ds_stopped', "1", this.liveTr.dynSub.settings.stop_on_close)
        } else if (type == 'pr') {
            this.liveTr.prodRecom.stopped = true;
            this.setCookie('_ra_pr_stopped', "1", this.liveTr.prodRecom.settings.stop_on_close)
        }
    },
    tryLiveTr: function(opt) {
        this.countLiveTrPage();
        if (!this.liveTr.stopped) {
            if (this.liveTr.progress === false) {
                if (!this.liveTr.loaded) {
                    if (!this.liveTr.dynSub.stopped) {
                        if (this.liveTr.count > 0 && this.liveTr.count % parseInt(this.liveTr.dynSub.settings.next_pages) == 0) {
                            this.liveTr.loaded = true;
                            this.setLiveTrProgress('ds');
                            var self = this;
                            setTimeout(function() {
                                self.sendLiveTr = [];
                                self.sendLiveTr.push("displayLiveTr=dynamic_subscription");
                                self.sendLiveTr.push("type=" + self.liveTr.dynSub.settings.type);
                                self.liveTrRequest()
                            }, self.liveTr.dynSub.settings.display_seconds * 1000)
                        }
                    }
                }
                if (!this.liveTr.loaded) {
                    if (!this.liveTr.prodRecom.stopped) {
                        if (this.liveTr.count > 0 && this.liveTr.count % parseInt(this.liveTr.prodRecom.settings.next_pages) == 0) {
                            this.liveTr.loaded = true;
                            this.setLiveTrProgress('pr');
                            var self = this;
                            setTimeout(function() {
                                self.sendLiveTr = [];
                                self.sendLiveTr.push("displayLiveTr=live_product_recommender");
                                self.sendLiveTr.push("type=" + self.liveTr.prodRecom.settings.type);
                                self.sendLiveTr.push("message=1");
                                self.liveTrRequest()
                            }, self.liveTr.prodRecom.settings.display_seconds * 1000)
                        }
                    }
                }
            } else if (this.liveTr.progress == 'pr') {
                if (!this.liveTr.loaded) {
                    if (this.liveTr.prodRecom.message == 2) {
                        this.liveTr.loaded = true;
                        var self = this;
                        setTimeout(function() {
                            self.sendLiveTr = [];
                            self.sendLiveTr.push("displayLiveTr=live_product_recommender");
                            self.sendLiveTr.push("type=" + self.liveTr.prodRecom.settings.type);
                            self.sendLiveTr.push("message=2");
                            self.liveTrRequest()
                        }, self.liveTr.prodRecom.settings.display_seconds * 1000)
                    }
                }
                if (!this.liveTr.loaded) {
                    if (this.liveTr.prodRecom.message == 3) {
                        if (opt !== undefined && opt.ids !== undefined && opt.ids instanceof Array) {
                            var ok = 0;
                            for (var prop in opt.ids) {
                                if (opt.ids[prop] == this.liveTr.prodRecom.prod_id) {
                                    ok = 1;
                                    break
                                }
                            }
                            if (ok == 1) {
                                this.liveTr.loaded = true;
                                var self = this;
                                setTimeout(function() {
                                    self.sendLiveTr = [];
                                    self.sendLiveTr.push("displayLiveTr=live_product_recommender");
                                    self.sendLiveTr.push("type=" + self.liveTr.prodRecom.settings.type);
                                    self.sendLiveTr.push("message=3");
                                    self.liveTrRequest()
                                }, self.liveTr.prodRecom.settings.display_seconds * 1000)
                            } else {
                                this.notShowProdRecom(3)
                            }
                        }
                    }
                }
            }
        }
    },
    liveTrRequest: function() {
        this.sendLiveTr.push('bws=' + this.browser);
        this.insertToBuffer('liveTrRequest')
    },
    //DEBUGGER
    attachSelfToDebugger : function() {
        if (typeof __ra_debug === 'undefined' || __ra_debug === null) {
            return;
            console.log('__ra_debug');
        }

        if (typeof __ra_debug.ra_ready !== 'function') {
            return;
            console.log('__ra_debug_ready');
        }

        __ra_debug.ra_ready(this);
    },
    //DEBUGGER
    setLastUrl: function() {
        if (this.getCookie('_ra_lurl') != this.currentUrl) {
            this.setCookie('_ra_lurl', this.currentUrl, 2, 'm');
            this.send = ["setLastUrl=" + encodeURIComponent(this.currentUrl)];
            this.sendInfo()
        }
        this.lastUrl = this.getCookie('_ra_lurl')
    },
    setProductRequestUrl: function(url) {
        this.send = [];
        this.send.push("setProductRequestUrl=" + url);
        this.sendInfo()
    },
    setEmailOrRefererFromUrl: function(_callback) {
        var lowerUrl = this.currentUrl.toLowerCase();
        var tokens = ["utm_ra=", "uem="];
        for (var i = 0; i < tokens.length; i++) {
            var token = tokens[i];
            if (lowerUrl.indexOf(token) > 0) {
                var temp = lowerUrl.split(token);
                var email = temp[temp.length - 1].split("&")[0].replace("%40", "@");
                if (lowerUrl.indexOf("ref_ra=") > 0) {
                    temp = lowerUrl.split("ref_ra=");
                    this.setEmail({
                        ref: temp[temp.length - 1].split("&")[0],
                        email: email
                    }, _callback)
                } else {
                    this.setEmail({
                        email: email
                    }, _callback)
                }
                break
            }
        }
    },
    randomInRange: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min)
    },
    consoleLogAllCookies: function() {
        var ca = document.cookie.split(';');
        console.log('---- cookies ----');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            console.log(c)
        }
        console.log('---- end cookies ----');
        return false
    },
    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length)
        }
        return ""
    },
    setCookie: function(cname, cvalue, exdays, um) {
        if (exdays === undefined) {
            exdays = 1
        }
        if (um === undefined) {
            um = 'd'
        }
        var d = new Date();
        if (um == 'd') {
            d.setTime(d.getTime() + (exdays * 86400000))
        } else if (um == 'h') {
            d.setTime(d.getTime() + (exdays * 36000000))
        } else if (um == 'm') {
            d.setTime(d.getTime() + (exdays * 60000))
        } else if (um == 's') {
            d.setTime(d.getTime() + (exdays * 1000))
        }
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;domain=" + _ra_req_folder
    },
    insertToBuffer: function(type, opt) {
        var newInBuffer = {
            type: type,
            get_data: undefined
        };
        if (type == 'sendInfo') {
            newInBuffer.get_data = this.send.join('&')
        } else if (type == 'liveTrRequest') {
            newInBuffer.get_data = this.sendLiveTr.join('&')
        }
        if (this.buffer_semaphore == 'go') {
            this.sendFromBuffer(newInBuffer);
            this.buffer_semaphore = 'stop';
            return
        }
        if (opt !== undefined && opt.pos !== undefined) {
            if (opt.pos == "start") {
                this.arr_buffer.unshift(newInBuffer);
                return
            }
        }
        this.arr_buffer.push(newInBuffer)
    },
    sendFromBuffer: function(buffer_data) {
        if (buffer_data.type == 'sendInfo') {
            var script = document.createElement('script');
            script.type = 'application/javascript';
            script.async = true;
            script.src = this.request + '?' + String(buffer_data.get_data).replace('bws=' + this.browser, 'bws=' + this.browser + '&' + 'sesid=' + this.session_id);
            var where = document.getElementsByTagName('script')[0];
            where.parentNode.insertBefore(script, where)
        } else if (buffer_data.type == 'liveTrRequest') {
            var ra2 = document.createElement('script');
            ra2.type = 'application/javascript';
            ra2.async = true;
            ra2.src = this.requestLiveTr + '?' + String(buffer_data.get_data).replace('bws=' + this.browser, 'bws=' + this.browser + '&' + 'sesid=' + this.session_id);
            var s2 = document.getElementsByTagName('script')[0];
            s2.parentNode.insertBefore(ra2, s2)
        }
    },
    sendInfo: function(_callback, opt) {
        if (this.send.length > 0) {
            this.send.push('bws=' + this.browser);
            if (_callback != null && _callback != undefined) {
                this.callbacks.push(_callback);
                this.send.push('callback=' + (this.callbacks.length - 1))
            } else {
                this.send.push('callback=0')
            }
            this.insertToBuffer('sendInfo', opt)
        }
    },
    sendInfoCallback: function(session_id, _callback_key, email) {
        this.setSessionId(session_id);
        if (_callback_key !== undefined && _callback_key > 0 && this.callbacks[_callback_key] !== undefined && this.callbacks[_callback_key] !== null) {
            if (typeof this.callbacks[_callback_key] === "function") {
                this.callbacks[_callback_key]()
            }
        }
        this.callbackEmail(email);
        if (this.arr_buffer.length > 0) {
            var data = this.arr_buffer.splice(0, 1);
            this.sendFromBuffer(data[0])
        } else {
            this.buffer_semaphore = 'go'
        }
    },
    callbackEmail: function(email) {
        var _ra_em = this.getCookie('_ra_em');
        var has_cookie = _ra_em.length > 0 ? true : false;
        if (email !== undefined && email !== null) {
            if (!has_cookie || _ra_em != email) {
                this.setCookie('_ra_em', email, 180)
            }
            return
        }
        if (!this.emailFromCookie && has_cookie) {
            this.emailFromCookie = true;
            this.setEmailFromCookie({
                email: _ra_em
            })
        }
    },
    sendScore: function(_s, id) {
        if (this.prodId !== undefined || id !== undefined) {
            this.send = [];
            this.send.push("id=" + (this.prodId !== undefined ? this.prodId : id));
            this.send.push("_s=" + _s);
            this.sendInfo()
        }
    },
    setEmail: function(info, _callback) {
        this.send = [];
        if (info !== undefined) {
            if (info.ref !== undefined) this.send.push("ref=" + info.ref);
            if (info.email_ref !== undefined) this.send.push("email_ref=" + info.email_ref);
            if (info.email !== undefined) this.send.push("setEmail=" + info.email);
            if (info.name !== undefined) this.send.push("name=" + info.name);
            if (info.phone !== undefined) this.send.push("phone=" + info.phone);
            if (info.city !== undefined) this.send.push("city=" + info.city);
            if (info.sex !== undefined) this.send.push("sex=" + info.sex)
        }
        this.sendInfo(_callback)
    },
    setEmailFromCookie: function(info) {
        this.send = [];
        if (info !== undefined) {
            if (info.email !== undefined) this.send.push("setEmailFromCookie=" + info.email)
        }
        this.sendInfo(undefined, {
            pos: "start"
        })
    },
    sendProduct: function(info, _callback) {
        this.send = [];
        if (info !== undefined) {
            if (info.id !== undefined) {
                this.prodId = info.id;
                this.send.push("id=" + info.id)
            }
            var self = this;
            this.secSpentInt = setInterval(function() {
                self.secSpent++;
                if (self.secSpent == 30) self.sendScore('time_spent_30s');
                if (self.secSpent == 60) self.sendScore('time_spent_1m');
                if (self.secSpent == 120) self.sendScore('time_spent_2m');
                if (self.secSpent == 180) self.sendScore('time_spent_3m');
                if (self.secSpent == 300) self.sendScore('time_spent_5m');
                if (self.secSpent == 420) {
                    self.sendScore('time_spent_7m')
                }
            }, 1000);
            this.send.push("sendProduct=1");
            this.send.push("ev=top3");
            if (info.name !== undefined) this.send.push("name=" + encodeURIComponent(info.name));
            if (info.url !== undefined) this.send.push("url=" + encodeURIComponent(info.url));
            if (info.img !== undefined) this.send.push("img=" + info.img);
            if (info.price !== undefined) this.send.push("price=" + info.price);
            if (info.promo !== undefined) this.send.push("promo=" + info.promo);
            if (info.stock !== undefined) this.send.push("stock=" + info.stock);
            if (info.brand.name !== undefined) this.send.push("brand_name=" + info.brand.name);
            if (info.brand.id !== undefined) this.send.push("brand_id=" + info.brand.id);
            if (info.category.name !== undefined) this.send.push("category_name=" + info.category.name);
            if (info.category.id !== undefined) this.send.push("category_id=" + info.category.id);
            if (info.category.parent !== undefined) this.send.push("category_parent=" + (info.category.parent !== false ? info.category.parent : 0));
            if (info.category.parent !== undefined && info.category.parent !== false && info.category_breadcrumb !== undefined) {
                if (info.category_breadcrumb instanceof Array) {
                    for (var i in info.category_breadcrumb) {
                        if (info.category_breadcrumb[i].id !== undefined && info.category_breadcrumb[i].name !== undefined && info.category_breadcrumb[i].parent !== undefined) {
                            this.send.push("bc[" + i + "][i]=" + info.category_breadcrumb[i].id);
                            this.send.push("bc[" + i + "][n]=" + info.category_breadcrumb[i].name);
                            this.send.push("bc[" + i + "][p]=" + (info.category_breadcrumb[i].parent !== false ? info.category_breadcrumb[i].parent : 0))
                        }
                    }
                }
            }
        }
        this.sendInfo(_callback);
        this.tryLiveTr()
    },
    sendCategory: function(info, _callback) {
        this.send = [];
        if (info !== undefined) {
            this.send.push("sendCategory=1");
            this.send.push("ev=category");
            if (info.name !== undefined) this.send.push("category_name=" + encodeURIComponent(info.name));
            if (info.id !== undefined) this.send.push("category_id=" + info.id);
            if (info.parent !== undefined) this.send.push("category_parent=" + (info.parent !== false ? info.parent : 0));
            if (info.parent !== undefined && info.parent !== false && info.category_breadcrumb !== undefined) {
                if (info.category_breadcrumb instanceof Array) {
                    for (var i in info.category_breadcrumb) {
                        if (info.category_breadcrumb[i].id !== undefined && info.category_breadcrumb[i].name !== undefined && info.category_breadcrumb[i].parent !== undefined) {
                            this.send.push("bc[" + i + "][i]=" + info.category_breadcrumb[i].id);
                            this.send.push("bc[" + i + "][n]=" + info.category_breadcrumb[i].name);
                            this.send.push("bc[" + i + "][p]=" + (info.category_breadcrumb[i].parent !== false ? info.category_breadcrumb[i].parent : 0))
                        }
                    }
                }
            }
            this.sendInfo(_callback)
        }
        this.tryLiveTr()
    },
    sendBrand: function(info, _callback) {
        this.send = [];
        if (info !== undefined) {
            this.send.push("sendBrand=1");
            this.send.push("ev=brand");
            if (info.id !== undefined) this.send.push("brand_id=" + info.id);
            if (info.name !== undefined) this.send.push("brand_name=" + info.name);
            this.sendInfo(_callback)
        }
        this.tryLiveTr()
    },
    addToCart: function(id, variation, _callback) {
        this.send = [];
        this.send.push("ev=add_to_cart");
        if (id !== undefined) this.send.push("id=" + id);
        if (variation !== false) {
            if (variation.code !== undefined) {
                var code = variation.code.split('-');
                var count = 0;
                if (variation.details !== undefined) {
                    for (var i in code) {
                        if (variation.details[code[i]] !== undefined) {
                            if (variation.details[code[i]].category !== undefined && variation.details[code[i]].category_name !== undefined && variation.details[code[i]].value) {
                                this.send.push("_c[" + code[i] + "]=" + variation.details[code[i]].category);
                                this.send.push("_cn[" + code[i] + "]=" + variation.details[code[i]].category_name);
                                this.send.push("_v[" + code[i] + "]=" + variation.details[code[i]].value);
                                count++
                            }
                        }
                    }
                }
                if (count == code.length) {
                    this.send.push("v=" + variation.code);
                    this.sendInfo(_callback)
                }
            }
        } else {
            this.send.push("v=");
            this.sendInfo(_callback)
        }
    },
    setVariation: function(id, variation, _callback) {
        this.send = [];
        this.send.push("ev=set_variation");
        if (id !== undefined) this.send.push("id=" + id);
        if (variation !== undefined) {
            if (variation.code !== undefined) {
                var code = variation.code.split('-');
                var count = 0;
                if (variation.details !== undefined) {
                    for (var i in code) {
                        if (variation.details[code[i]] !== undefined) {
                            if (variation.details[code[i]].category !== undefined && variation.details[code[i]].category_name !== undefined && variation.details[code[i]].value) {
                                this.send.push("_c[" + code[i] + "]=" + variation.details[code[i]].category);
                                this.send.push("_cn[" + code[i] + "]=" + variation.details[code[i]].category_name);
                                this.send.push("_v[" + code[i] + "]=" + variation.details[code[i]].value);
                                count++
                            }
                        }
                    }
                }
                if (count == code.length) {
                    this.send.push("v=" + variation.code);
                    this.sendInfo(_callback)
                }
            }
        }
    },
    clickImage: function(id, _callback) {
        this.send = [];
        this.send.push("ev=click_image");
        if (id !== undefined) this.send.push("id=" + id);
        this.sendInfo(_callback)
    },
    likeFacebook: function(id, _callback) {
        this.send = [];
        this.send.push("ev=facebook_like");
        if (id !== undefined) this.send.push("id=" + id);
        this.sendInfo(_callback)
    },
    addToWishlist: function(id, _callback) {
        this.send = [];
        this.send.push("ev=wishlist");
        if (id !== undefined) this.send.push("id=" + id);
        this.sendInfo(_callback)
    },
    visitHelpPage: function(_callback) {
        this.send = [];
        this.send.push("ev=help_and_faq");
        this.sendInfo(_callback)
    },
    commentOnProduct: function(id, _callback) {
        this.send = [];
        this.send.push("ev=comment_on_product");
        if (id !== undefined) this.send.push("id=" + id);
        this.sendInfo(_callback)
    },
    dblClickPrice: function(id, info, _callback) {
        if (!this.dblClickPriceSent) {
            this.send = [];
            this.send.push("ev=dblclick_price");
            if (id !== undefined) this.send.push("id=" + id);
            if (info.price !== undefined) this.send.push("price=" + info.price);
            if (info.promo !== undefined) this.send.push("promo=" + info.promo);
            this.sendInfo(_callback)
        }
        this.dblClickPriceSent = true
    },
    saveOrder: function(info, products, _callback) {
        this.send = [];
        this.send.push("saveOrder=1");
        if (info.order_no !== undefined) this.send.push("order_no=" + info.order_no);
        if (info.lastname !== undefined) this.send.push("data[lastname]=" + info.lastname);
        if (info.firstname !== undefined) this.send.push("data[firstname]=" + info.firstname);
        if (info.email !== undefined) this.send.push("data[email]=" + info.email);
        if (info.phone !== undefined) this.send.push("data[phone]=" + info.phone);
        if (info.state !== undefined) this.send.push("data[state]=" + info.state);
        if (info.city !== undefined) this.send.push("data[city]=" + info.city);
        if (info.address !== undefined) this.send.push("data[address]=" + info.address);
        if (info.discount !== undefined) this.send.push("data[discount]=" + info.discount);
        if (info.discount_code !== undefined) this.send.push("data[discount_code]=" + info.discount_code);
        if (info.shipping !== undefined) this.send.push("data[shipping]=" + info.shipping);
        if (info.total !== undefined) this.send.push("data[total]=" + info.total);
        if (products !== undefined && products instanceof Array) {
            for (var prop in products) {
                if (products[prop].id !== undefined && products[prop].quantity !== undefined && products[prop].price !== undefined && products[prop].variation_code !== undefined) {
                    this.send.push("products[" + prop + "][i]=" + products[prop].id);
                    this.send.push("products[" + prop + "][q]=" + products[prop].quantity);
                    this.send.push("products[" + prop + "][p]=" + products[prop].price);
                    this.send.push("products[" + prop + "][v]=" + products[prop].variation_code)
                }
            }
        }
        this.sendInfo(_callback)
    },
    notShowDynSub: function() {
        this.setLiveTrProgress(false)
    },
    showDynSub: function() {
        this.setLiveTrProgress(false);
        this.send = [];
        this.send.push("visit_dyn_sub_ref=" + this.liveTr.dynSub.settings.type);
        this.send.push("ev=show_dynamic_subscription");
        this.sendInfo()
    },
    submitDynSub: function(sex) {
        if (sex === undefined || (sex !== 0 && sex !== 1)) {
            sex = 2
        }
        var input_email = document.getElementById("_ra-livetr-email");
        var input_name = document.getElementById("_ra-livetr-name");
        var input_phone = document.getElementById("_ra-livetr-phone");
        this.send = [];
        this.send.push("email_ref=dynamic_subscription");
        this.send.push("dyn_sub_ref=" + this.liveTr.dynSub.settings.type);
        this.send.push("visit_dyn_sub_ref=" + this.liveTr.dynSub.settings.type);
        if (input_email !== null && input_email !== undefined) {
            this.send.push("setEmail=" + input_email.value)
        }
        if (input_name !== null && input_name !== undefined) {
            this.send.push("name=" + input_name.value)
        }
        if (input_phone !== null && input_phone !== undefined) {
            this.send.push("phone=" + input_phone.value)
        }
        this.send.push("sex=" + sex);
        this.sendInfo();
        this.stopLiveTr('ds');
        _ra_modal.close()
    },
    closeDynSub: function() {
        this.send = [];
        this.send.push("ev=close_dynamic_subscription");
        this.sendInfo();
        this.stopLiveTr('ds');
        _ra_modal.close()
    },
    notShowProdRecom: function(msg) {
        this.setLiveTrProgress(false)
    },
    showProdRecom: function(msg) {
        this.send = [];
        this.send.push("visit_prod_recom_ref=" + this.liveTr.prodRecom.settings.type);
        this.send.push("visit_prod_recom_msg=" + msg);
        this.send.push("ev=show_live_product_recommender_" + msg);
        if (msg == 1) {
            this.liveTr.prodRecom.type = this.liveTr.prodRecom.settings.type;
            this.resetLiveTrCookie()
        }
        if (msg == 2) {
            this.liveTr.prodRecom.message = 3;
            this.resetLiveTrCookie()
        } else if (msg == 3) {
            this.setLiveTrProgress(false);
            this.stopLiveTr('pr')
        }
        this.sendInfo()
    },
    submitProdRecom: function(msg, options) {
        this.send = [];
        if (msg == 1) {
            this.send.push("visit_prod_recom_ref=" + this.liveTr.prodRecom.settings.type);
            this.send.push("visit_prod_recom_msg=" + msg);
            this.send.push("ev=submit_live_product_recommender_" + msg);
            if (options.id !== undefined && options.url !== undefined) {
                this.liveTr.prodRecom.count = this.liveTr.count;
                this.liveTr.prodRecom.prod_id = options.id;
                this.liveTr.prodRecom.message = 2;
                this.resetLiveTrCookie()
            }
            this.sendInfo(function() {
                window.location.href = options.url
            })
        }
    },
    closeProdRecom: function(msg) {
        this.send = [];
        this.send.push("ev=close_live_product_recommender_" + msg);
        this.sendInfo();
        if (msg == 1) {
            this.stopLiveTr('pr')
        }
        _ra_modal.close()
    },
    hideModalProdRecom: function() {
        _ra_modal.close()
    },
    checkoutIds: function(ids) {
        if (ids instanceof Array) {
            this.tryLiveTr({
                ids: ids
            })
        }
    }
};
_ra.init(_ra_temp, _ra_dyn_sub, _ra_prod_recom);
window.onscroll = function() {
    var w = window,
        d = document,
        e = d.documentElement,
        b = d.body,
        s = (w.pageYOffset || e.scrollTop) - (e.clientTop || 0),
        h = Math.max(b.scrollHeight, b.offsetHeight, e.clientHeight, e.scrollHeight, e.offsetHeight);
    if ((h / s) < 2 && !_ra.scrollSended && _ra.prodId !== undefined) {
        _ra.sendScore('page_scroll');
        _ra.scrollSended = true
    }
};
var _ra_modal = _ra_modal || {
        modal: {
            container: undefined,
            inner: undefined,
            x: undefined,
            y: undefined,
            interval: undefined
        },
        init: function(content, x, y) {
            if (document.getElementById('_ra-modal-container') == null) {
                var div = document.createElement("div");
                div.id = "_ra-modal-container";
                div.style.position = "fixed";
                div.style.opacity = "0";
                div.style.zIndex = "999999999";
                document.getElementsByTagName('body')[0].style.position = "relative";
                document.getElementsByTagName('body')[0].appendChild(div)
            }
            this.modal.x = x;
            this.modal.y = y;
            this.modal.container = document.getElementById('_ra-modal-container')
        },
        setContent: function(content) {
            var div = document.createElement("div");
            div.id = "_ra-modal-inner-container";
            div.innerHTML = content;
            this.modal.container.appendChild(div);
            this.modal.inner = document.getElementById('_ra-modal-inner-container')
        },
        repositions: function() {
            var w = this.modal.inner.offsetWidth;
            var h = this.modal.inner.offsetHeight;
            if (this.modal.x == 'center') {
                this.modal.container.style.left = (window.innerWidth - w) / 2 + "px"
            } else if (this.modal.x == 'left') {
                this.modal.container.style.left = "20px"
            } else {
                this.modal.container.style.right = "20px"
            }
            if (this.modal.y == 'middle') {
                this.modal.container.style.top = (window.innerHeight - h) / 2 + "px"
            } else if (this.modal.y == 'bottom') {
                this.modal.container.style.bottom = "20px"
            } else {
                this.modal.container.style.top = "20px"
            }
        },
        show: function() {
            this.modal.container.style.opacity = "1"
        },
        close: function() {
            this.modal.container.parentNode.removeChild(this.modal.container)
        },
        display: function(content, x, y) {
            this.init(content, x, y);
            this.setContent(content);
            this.repositions();
            this.show()
        },


    };