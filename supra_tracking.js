// js cookie
!function(a){var b=!1;if("function"==typeof define&&define.amd&&(define(a),b=!0),"object"==typeof exports&&(module.exports=a(),b=!0),!b){var c=window.Cookies,d=window.Cookies=a();d.noConflict=function(){return window.Cookies=c,d}}}(function(){function a(){for(var a=0,b={};a<arguments.length;a++){var c=arguments[a];for(var d in c)b[d]=c[d]}return b}function b(c){function d(b,e,f){var g;if("undefined"!=typeof document){if(arguments.length>1){if(f=a({path:"/"},d.defaults,f),"number"==typeof f.expires){var h=new Date;h.setMilliseconds(h.getMilliseconds()+864e5*f.expires),f.expires=h}try{g=JSON.stringify(e),/^[\{\[]/.test(g)&&(e=g)}catch(a){}return e=c.write?c.write(e,b):encodeURIComponent(String(e)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),b=encodeURIComponent(String(b)),b=b.replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent),b=b.replace(/[\(\)]/g,escape),document.cookie=[b,"=",e,f.expires?"; expires="+f.expires.toUTCString():"",f.trackingPath?"; path="+f.trackingPath:"",f.domain?"; domain="+f.domain:"",f.secure?"; secure":""].join("")}b||(g={});for(var i=document.cookie?document.cookie.split("; "):[],j=/(%[0-9A-Z]{2})+/g,k=0; k<i.length; k++){var l=i[k].split("="),m=l.slice(1).join("=");'"'===m.charAt(0)&&(m=m.slice(1,-1));try{var n=l[0].replace(j,decodeURIComponent);if(m=c.read?c.read(m,n):c(m,n)||m.replace(j,decodeURIComponent),this.json)try{m=JSON.parse(m)}catch(a){}if(b===n){g=m;break}b||(g[n]=m)}catch(a){}}return g}}return d.set=d,d.get=function(a){return d.call(d,a)},d.getJSON=function(){return d.apply({json:!0},[].slice.call(arguments))},d.defaults={},d.remove=function(b, c){d(b,"",a(c,{expires:-1}))},d.withConverter=b,d}return b(function(){})});

_SUPRA_TRACKING = function () {
    // to change in production
    this.trackingDomain="http://trk.local:8090";
    // this.trackingDomain="https://plugins.retargeting.biz";
    // this.trackingPath="/lvtr-debug/trk.php";
    this.trackingPath="/trk.php";

    // Modal: TO CUSTOMIZE
    this.modalClass = '__ra-modal-box';

    // LPR: TO CUSTOMIZE
    this.filterLprModal1Shown = 'submitProdRecom(1';
    this.filterLprModal2Shown = 'closeProdRecom(2';
    this.filterLprModal3Shown = 'closeProdRecom(3';

    // CS: TO CUSTOMIZE
    this.filterCsModal1Shown = 'submitCartSaver(1';
    this.filterCsModal2Shown = 'closeCartSaver(2';

    // DS
    this.filterDsModal1Shown = 'submitDynSub(2';

    // TO CUSTOMIZE
    this.productVersion = 'v3';

    // cookie tracking
    this.trackingCookieName = '__ra_supra';
    this.trackingMediumName = '__ra_mediums';
    this.trackingSequenceName = '__ra_sequence';

    var that = this;

    this.modalText = function(modal) {
        return {
            'content': that.utils.strip_tags(modal[0].innerHTML)
        };
    };

    // TO CUSTOMIZE
    this.parseFirstModal = function(modal) {
        return {
            'promoText': modal.find("p")[0].innerHTML,
            'price': modal.find("p")[2].innerHTML
        };
    };

    // TO CUSTOMIZE
    this.parseSecondModal = function(modal) {
        return {
            'promoText': modal.find("p")[0].innerHTML
        };
    };

    this.debug = function() {
        // console.log(arguments);
    };

    this.track = function (event_name, attrs) {
        // time tracking
        this.trackingSequence = Cookies.get(this.trackingSequenceName);
        if(!this.trackingSequence) {
            this.trackingSequence = 0;
        }
        this.trackingSequence = parseInt(this.trackingSequence);
        this.trackingSequence += 1;
        Cookies.set(this.trackingSequenceName, this.trackingSequence, { expires: 365 });
        attrs['timeSequence'] = this.trackingSequence;
        attrs['time'] = (new Date()).toISOString();

        // attrs enhancement
        attrs['productVersion'] =this.productVersion;
        attrs['currentUrl'] = window.location.href;
        attrs['refererUrl'] = document.referrer;
        attrs['userAgent'] = navigator.userAgent;
        attrs['trackingId'] = this.trackingId;
        attrs['trackingMediums'] = JSON.stringify(this.trackingMediums);
        attrs['rtCookies'] = {
            '__ra': Cookies.get('__ra'),
            '__ralv': Cookies.get('__ra'),
            '_ra_sesid': Cookies.get('_ra_sesid'),
            '_ra_lvtr': Cookies.get('_ra_lvtr')
        };

        this.debug("--> TRACK:", event_name, attrs);

        this.utils.sendInfo(event_name, attrs);
    };

    // text = jQuery('#__ra-modal-container-area3 p')[0];
    this.linksMethodOnClick = function (parent, methodName) {
        return parent.find("a").filter(function (_, a) {
            if (a.onclick) {
                var functionCode = a.onclick.toString();
                if (functionCode.indexOf(methodName) >= 0) {
                    // function onclick(event) {
                    //     _ra.submitProdRecom(1, {id: '3976', url: 'http://shop.yellowstore.ro/camere/the-game?utm_source=Retargeting.biz&utm_medium=live&utm_campaign=Retargeting.ro-20161103'})
                    // }
                    return true;
                }
            }
        })
    };

    this.onModalDisplayed = function () {
        var retaModal = that.modal();
        this.debug("reta modal showed up: ", retaModal);

        // are we on first step?
        if (that.linksMethodOnClick(
                retaModal, that.filterLprModal1Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('lpr.step1.modal_shown', attrs)
        } else if (
            that.linksMethodOnClick(
                retaModal, that.filterLprModal2Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('lpr.step2.modal_shown', attrs)

        } else if (
            that.linksMethodOnClick(
                retaModal, that.filterLprModal3Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('lpr.step3.modal_shown', attrs)

        } else if (
            that.linksMethodOnClick(
                retaModal, that.filterCsModal1Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('cs.step1.modal_shown', attrs)
        }
        else if (
            that.linksMethodOnClick(
                retaModal, that.filterCsModal2Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('cs.step2.modal_shown', attrs)
        }
        else if (
            that.linksMethodOnClick(
                retaModal, that.filterDsModal1Shown).length != 0) {
            var attrs = that.modalText(retaModal);
            this.track('ds.step1.modal_shown', attrs)
        }
    };

    this.onAddToCart = function() {
        this.track('lpr.step3.add_to_cart', attrs)
    };

    this.utils = {
        strip_tags: function (input, allowed) {
            // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
            allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');

            var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
            var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

            return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            })
        },

        stopTrack: function () {
            var user_agent = navigator.userAgent;
            var bot_pattern = "(googlebot\/|Googlebot-Mobile|Googlebot-Image|Google favicon|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)";
            var re = new RegExp(bot_pattern, 'i');
            // if it is a bot
            if (re.test(user_agent)) {
                return true;
            }
            return false;
        },

        sendInfo: function (eventName, eventParams) {
            var trkImg = new Image();
            var ctxId = Math.random(1).toString(36).substring(7);
            trkImg.src = that.trackingDomain +
                that.trackingPath + "?ctx=" + ctxId + "&" +
                this.toParams(eventName, eventParams);
        },

        toParams: function (eventName, eventData) {
            var out = [("event_type=" + eventName)];
            for (var key in eventData) {
                var value = eventData[key];
                var isObject = value !== null && typeof value === 'object';
                if(isObject) {
                    value = JSON.stringify(value); }
                out.push(key + '=' + encodeURIComponent(value));
            }
            return out.join('&');
        },

        queryParams: function (qs) {
            var urlParams;
            var match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) {
                    return decodeURIComponent(s.replace(pl, " "));
                },
                query = window.location.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
                urlParams[decode(match[1])] = decode(match[2]);
            return urlParams;
        },

        onlyQueryParams: function (b) {
            var qd = {};
            b.split("&").forEach(function(item) {
                var s = item.split("="),
                    k = s[0],
                    v = s[1] && decodeURIComponent(s[1]);
                //(k in qd) ? qd[k].push(v) : qd[k] = [v]
                // (qd[k] = qd[k] || []).push(v) //short-circuit
                qd[k] = v;
            });
            return qd;
        },


        arraysEqual: function arraysEqual(a, b) {
            if (a === b) return true;
            if (a == null || b == null) return false;
            if (a.length != b.length) return false;

            // If you don't care about the order of the elements inside
            // the array, you should sort both arrays here.

            for (var i = 0; i < a.length; ++i) {
                if (a[i] !== b[i]) return false;
            }
            return true;
        }

    };

    this.modal = function() {
        return jQuery("div." + this.modalClass).length ? jQuery("div." + this.modalClass) : jQuery("div#_ra-modal-container");
    };

    this.trackCallsDecorator = function (f, fName) {        // (1)
        return function() {
            var args = Array.prototype.slice.call(arguments);
            args['name'] = fName;
            that.track('reta.method_called', {args: args});
            return f.apply(this, arguments)
        }
    };

    this.onDomNodeAppear = function(selector, callback) {
        // make it loop every 200 milliseconds
        var interval = setInterval(function(){
            if(jQuery(selector).length > 0) {
                clearInterval(interval);
                setTimeout(callback, 0);
            }
        }, 200); // every 200 milliseconds
    };

    this.ra_ready = function(ra_instance) {
        // console.log("Will try to intercept ra instance");
        if (typeof ra_instance !== 'undefined' && ra_instance.ready !== undefined) {
            console.log("Intercepted _ra instance");

            if(ra_instance.sendFromBuffer) {
                console.log("V2 ra instance");
                // v2
                var _old = ra_instance.sendFromBuffer.bind(ra_instance);
                ra_instance.sendFromBuffer = function(buffer_data) {
                    var args = that.utils.onlyQueryParams(JSON.parse(JSON.stringify(buffer_data.get_data)));
                    args['name'] = args['ev'];
                    // console.log("args: ", args);
                    that.track('reta.method_called', {args: args});

                    return _old(buffer_data);
                }
            } else {
                // v3
                console.log("V3 ra instance");
                ra_instance.addToCart = this.trackCallsDecorator(ra_instance.addToCart, "addToCart");
                ra_instance.sendProduct = this.trackCallsDecorator(ra_instance.sendProduct, "sendProduct");
                ra_instance.request.call = this.trackCallsDecorator(ra_instance.request.call, "raCall");
            }


        }
    };

    this.tagUser = function() {
        // load tracking context
        this.trackingId = Cookies.get(this.trackingCookieName);
        if(!this.trackingId) {
            this.trackingId = Math.random(1).toString(36).substring(7);
        }
        Cookies.set(this.trackingCookieName, this.trackingId, { expires: 365 });
        // console.log("ID is: ", this.trackingId);

        // track mediums
        var currentHref = window.location.href + "";
        var parsedLocation = this.utils.queryParams(currentHref);

        this.trackingMediums = Cookies.get(this.trackingMediumName);
        if(!this.trackingMediums) {
            this.trackingMediums = "[]";
        }
        this.trackingMediums = JSON.parse(this.trackingMediums);
        if(parsedLocation.utm_medium && parsedLocation.utm_source) {
            var lastMediums = this.trackingMediums.slice(-1).pop();
            var currentMediums = [parsedLocation.utm_medium, parsedLocation.utm_source];
            if(!this.utils.arraysEqual(lastMediums, currentMediums)) {
                this.trackingMediums.push(currentMediums);
            }
        }
        Cookies.set(this.trackingMediumName, JSON.stringify(this.trackingMediums),
            {expires: 365});
    };

    this.init = function () {
        if(this.utils.stopTrack()) {
            return;
        }

        this.tagUser();

        // log current page
        this.track('common.page_visited', {});

        // hook on first modal
        doc = jQuery(document);
        this.onDomNodeAppear("div." + this.modalClass, this.onModalDisplayed.bind(this));
        this.onDomNodeAppear("div#_ra-modal-container", this.onModalDisplayed.bind(this));
        // this.addToCartSelector = "." + _ra_preload_options.add_to_cart_selector;
        // this.onDomNodeAppear(this.addToCartSelector, this.onAddToCart);

        // jQuery(document).ready(function() {
        //     that.onDomReady();
        // });
    };

    // initializer
    this.init();
};

__ra_debug = new _SUPRA_TRACKING();
console.log("supra_tracking loaded.");
